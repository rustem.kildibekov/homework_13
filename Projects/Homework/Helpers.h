#pragma once

double foo(double first, double second) {
	double sum = first + second;
	return sum * sum;
}